import { useContext } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext.js";

export default function AppNavbar(){

  const { user } = useContext(UserContext);

  return(
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">CompCor</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav>
            <Nav.Link className="mx-3" as={Link} to="/">Home</Nav.Link>
            <Nav.Link className="mx-3" as={Link} to="/products">Products</Nav.Link>
            {
              (user.isAdmin)
              ?
              <>
              <Nav.Link className="mx-3" as={Link} to="/orders">Orders</Nav.Link>
              <Nav.Link className="mx-3" as={Link} to="/users">Users</Nav.Link>
              </>
              :
              <></>
            }
            <Nav.Link disabled> | </Nav.Link>
            {
              (user.id !== null) 
              ?
              <Nav.Link className="mx-3" as={Link} to="/logout">Logout</Nav.Link>
              :
              <>
                <Nav.Link className="mx-3" as={Link} to="/login">Login</Nav.Link>
                <Nav.Link className="mx-3" as={Link} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}
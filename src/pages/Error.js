import Banner from "../components/Banner.js";
// import {Row, Col, Button} from "react-bootstrap";
// import {Link} from "react-router-dom";

export default function Error(){
	
	const data = {
		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

	return(
		// "prop" name is up to the developer.
		// Best practice is to use the base name + "Prop", thus, bannerProp in this case.
		<Banner bannerProp={data} />
	)
}
import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext.js";

export default function Logout(){

	// Consume the UserContext Object and destructure it to access the user state and unsetUser function from our provider.
	const { unsetUser, setUser } = useContext(UserContext);

	// Clear the localStorageor user's information.
	unsetUser();

	useEffect(() => {
		// Set the user state back to its original value.
		setUser({id: null});
	});


	// The "lcoalStorage.clear()" method allows us to clear the information in the localStorage ensuring no information is stored in our browser.
	// In order to see this effect, we will need to refresh the browser for us to see the changes in our application.
	// Closing and reopening the browser and even restarting our devices will make the information in the localStorage persistent.
	// It's always good practice to include code in our application to clear the localStorage when it is no longer needed.
	
	/*localStorage.clear();*/

	return(
		<Navigate to="/login"/>
	)
}
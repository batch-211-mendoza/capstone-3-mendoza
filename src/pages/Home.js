import Banner from "../components/Banner";
// import Highlights from "../components/Highlights";

export default function Home(){

	const data = {
			title: "CompCor - Computer Corner",
			content: "The place where you can buy the only set of computers you'll ever need.",
			destination: "/products",
			label: "Shop Now!"
		}

	return(
		<>
			<Banner bannerProp={data}/>
			{/*<Highlights/>*/}
		</>
	)
}
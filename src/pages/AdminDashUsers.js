import { useEffect, useState, useContext } from "react";
import { Button, Table } from "react-bootstrap";
import { Navigate } from "react-router-dom";

import UserContext from "../UserContext.js";
import Swal from "sweetalert2";

export default function AdminDash(){

	const { user } = useContext(UserContext);

	const [allUsers, setAllUsers] = useState([]);

	const fetchUsers = () =>{


		fetch(`https://capstone-2-mendoza.onrender.com/users`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return(
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName + " " + user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNo}</td>
						<td>{user.isAdmin ? "Admin" : "User"}</td>
						<td className="d-flex flex-column">
							{
								(user.isAdmin)
								?	
								<Button className="w-100 my-1" variant="success" size="sm" onClick ={() => setUser(user._id, user.firstName)}>
									Set as User
								</Button>
								:
								<>
									<Button className="w-100 my-1" variant="danger" size="sm" onClick ={() => setAdmin(user._id, user.firstName)}>
										Set as Admin
									</Button>
								</>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	const setUser = (userId, userFirstName) =>{

		fetch(`https://capstone-2-mendoza.onrender.com/users/${userId}/setRole`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Role Change Succesful!",
					icon: "success",
					text: `${userFirstName}'s role has been set to "User".`
				})
				fetchUsers();
			}
			else{
				Swal.fire({
					title: "Role Change Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const setAdmin = (userId, userFirstName) =>{

		fetch(`https://capstone-2-mendoza.onrender.com/users/${userId}/setRole`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(response => response.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Role Change Succesful!",
					icon: "success",
					text: `${userFirstName}'s role has been set to "Admin".`
				})
				fetchUsers();
			}
			else{
				Swal.fire({
					title: "Role Change Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{
		fetchUsers();
	},[])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard - Users</h1>
			</div>
			<Table striped bordered hover>
		     <thead className="py-5 my-5">
		       <tr>
		         <th>User ID</th>
		         <th>Name</th>
		         <th>Email Address</th>
		         <th>Mobile No.</th>
		         <th>Role</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products"/>

	)
}
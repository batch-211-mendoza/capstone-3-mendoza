import { useEffect, useState, useContext } from "react";
import { Button, Table, Modal } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";

import AdminAddProduct from "../components/AdminAddProduct.js";

import UserContext from "../UserContext.js";
import Swal from "sweetalert2";

export default function AdminDash(){

	const { user } = useContext(UserContext);

	const [showAdd, setShowAdd] = useState(false);
	const handleCloseAdd = () => setShowAdd(false);
	const handleShowAdd = () => setShowAdd(true);

	const [allProducts, setAllProducts] = useState([]);

	const fetchProducts = () =>{
		fetch(`https://capstone-2-mendoza.onrender.com/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td className="d-flex flex-column">
							{
								(product.isActive)
								?	
								<Button className="w-100 my-1" variant="success" size="sm" onClick ={() => archive(product._id, product.name)}>
									Archive
								</Button>
								:
								<>
									<Button className="w-100 my-1" variant="danger" size="sm" onClick ={() => unarchive(product._id, product.name)}>
										Activate
									</Button>
								</>
							}
							<Button as={ Link } to={`/products/${product._id}/update`} variant="secondary" size="sm" className="my-1 w-100" >Edit</Button>
						</td>
					</tr>
				)
			}))
		})
	}

	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://capstone-2-mendoza.onrender.com/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(response => response.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchProducts();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`http://localhost:4000/products/${productId}/activate`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(response => response.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchProducts();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{
		fetchProducts();
	},[])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard - Products</h1>
				<Button onClick={handleShowAdd} variant="primary" size="lg" className="mx-2">
					Add Product
				</Button>
			</div>
			<Table striped bordered hover>
		     <thead className="py-5 my-5">
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>

		<Modal show={showAdd} onHide={handleCloseAdd}>
			<AdminAddProduct closeAddProp={handleCloseAdd}/>;
		</Modal>
		</>
		:
		<Navigate to="/products"/>

	)
}
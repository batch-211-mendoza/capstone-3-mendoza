import { useEffect, useState, useContext } from "react";
import { Button, Table, Modal } from "react-bootstrap";
import { Navigate } from "react-router-dom";

import UserContext from "../UserContext.js";
import Swal from "sweetalert2";

export default function AdminDash(){

	const { user } = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);

	const fetchOrders = () => {


		fetch(`https://capstone-2-mendoza.onrender.com/orders`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.firstName + " " + order.lastName}</td>
						<td>{order.email}</td>
						<td>{order.mobileNo}</td>
						<td>{order.isAdmin ? "Admin" : "User"}</td>
						<td className="d-flex flex-column">
							{
								(user.isAdmin)
								?	
								{/*<Button className="w-100 my-1" variant="success" size="sm" onClick ={() => setOrder(order._id, user.firstName)}>
									Set as User
								</Button>*/}
								:
								<>
									{/*<Button className="w-100 my-1" variant="danger" size="sm" onClick ={() => setAdmin(order._id, user.firstName)}>
										Set as Admin
									</Button>*/}
								</>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(()=>{
		fetchOrders();
	},[])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard - Orders</h1>
			</div>
			<Table striped bordered hover>
		     <thead className="py-5 my-5">
		       <tr>
		         <th>User ID</th>
		         <th>Name</th>
		         <th>Email Address</th>
		         <th>Mobile No.</th>
		         <th>Role</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products"/>

	)
}